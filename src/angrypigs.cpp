/**
 * Angry Pigs is a specialized antivirus program designed for the destruction
 * of and recovery from the Angry Birds virus. The program has been tested
 * on Windows XP, Windows 7, and Windows 8.
 *
 * In addition to deleting all copies of the Angry Birds virus itself
 * ("angry birds.vbe"), the program detects shortcuts, executables, and
 * system files created by angry birds and unhides files hidden by the
 * virus. Shortcuts not created by Angry Birds will not be harmed, though
 * the program has no way of detecting files that were hidden before Angry
 * Birds did its work.
 *
 * Typically, this program will sweep over all programs on the machine. This
 * is the recommended use, as the program will not harm your normal shortcuts,
 * hidden files, or anything else on your filesystem. However, if necessary,
 * you can localize it to a smaller subset of your files through the 
 * -l flag.
 *
 * Copyright (C) 2014 Jacob Collard
 *
 * This is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public LIcense as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see
 * <a href="http://www.gnu.org/licenses/">the online version</a>
 */

#include <fstream>
#include <iostream>
#include <map>
#include <new>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <string.h>
#include <sstream>
#include <vector>
#include <windows.h>

#define VB_COUNT 1
#define AB_CHARS 43
#define SC_CHARS 21
#define EX_CHARS 10
#define INI_CHARS 7

using namespace std;

char AB_ID[AB_CHARS] = {'#', '@', '~', 'Y', 'O', 'g', 'A', 'A', 'A', '=',
                         '=', 'v', '@', '!', 'P', 'R', '1', 'X', '(', '+',
                         'D', ',', 'Z', 'M', 'z', 'w', 'D', '+', '.', '0', 
                         ',', '@', '*', '@', '#', '@', '&', 'D', 'F', 'P', 
                         '{', '~', 't'};
char SC_ID[SC_CHARS] = {'s', '\0', 't', '\0', 'a', '\0', 'r', '\0', 't', 
                         '\0', ' ', '\0', 'a', '\0', 'n', '\0', 'g', '\0', 
                         'r', '\0', 'y'};
char EXE_ID[EX_CHARS] = {'v', 'b', 'a', 'O', 'n', 'E', 'r', 'r', 'o', 'r'};
char INI_ID[INI_CHARS] = {'S', 'h', 'e', 'l', 'l', '3', '2'};

bool terminate_vbs();
void find_malicious(string filesystem, vector<string>* virus_files,
    vector<string>* shortcut_files);
void find_mal_in_subdirs(string dir, vector<string>* virus_files, 
    vector<string>* shortcut_files);
short destroy_shortcut(string target);
string remove_extension(string str);
bool destroy_virus(string target);
vector<string> &split(const string &s, char delim, vector<string> &elems);
vector<string>* split(const string &s, char delim);
string exec(const char* cmd);
string to_upper_copy(string str);
bool string_in_file(char* to_find, int size, const char* filename);
string strip_copy(string to_strip);

const char* vbs_programs[VB_COUNT] = {"wscript.exe"};

int main(int argc, char* argv[]) {
  /**
   * The basic flow of the program is as follows:
   * 1.) Terminate all of the commonly used VBScript interpreters. Note
   * that this step CAN interfere with other programs running - make sure
   * all VBScript programs have been stopped gracefully before running
   * Angry Pigs.
   * 2.) Search the system for malicious files. This will obtain a list of
   * files which are either a.) angry birds vbe files, b.) .lnk files
   * created by Angry Birds, c.) .exe files created by Angry Birds, or
   * d.) .ini files created by Angry Birds.
   * 3.) Go through the list of malicious files and destroy them. In the
   * case of .lnk  and .exe files, the original file is found and unhidden. 
   * This can potentially unhide files with the same name that were also 
   * hidden but had different extensions. There is no way of knowing which 
   * files were hidden before angry birds, so some additional files may be 
   * visible that were not visible before angry birds struck. Angry Birds vbe 
   * files are deleted without question.
   */

  string filesystem;
  short code;

  if (argc == 3 && (strcmp(argv[1], "-l") == 0)) {
    filesystem = argv[2];
  }
  else {
    filesystem = "ALL";
  }

  // Step 1: Terminate all VBScript interpreters that may be running Angry
  // Birds.
  if (!terminate_vbs()) {
    cerr << "No copies of Angry Birds are currently running." << endl;
  }

  // Step 2: Locate all malicious files and create vectors of files which
  // are angry birds and malicious .ini files and a list of files which
  // are .lnk or .exe files. VBE and ini files can be deleted outright,
  // but .exe and .lnk files require the restoration of hidden files.
  vector<string>* virus_files = new vector<string>();
  vector<string>* shortcut_files = new vector<string>();
  find_malicious(filesystem, virus_files, shortcut_files);

  // Step 3a: Destroy all copies of angry birds vbe files and ini files
  for (unsigned int i=0 ; i<virus_files->size() ; i++) {
    if (!destroy_virus(virus_files->at(i))) {
      cerr << "Could not destroy virus at " << virus_files->at(i) << endl;
    }
  }

  // Step 3b: Destroy all malicious shortcuts and applications and
  // restore originals
  for (unsigned int i=0 ; i<shortcut_files->size() ; i++) {
    code = destroy_shortcut(shortcut_files->at(i));
    if (code == 1) {
      cerr << "Failed to destroy shortcut at " <<
        shortcut_files->at(i) << endl;
    }
    if (code == 2) {
      cerr << "Failed to restore original version of " <<
        shortcut_files->at(i) << endl;
    }
    if (code == 3) {
      cerr << "Failed to destroy shortcut at " <<
        shortcut_files->at(i) << endl;
      cerr << "Failed to restore original version of " <<
        shortcut_files->at(i) << endl;
    }
  }

  delete virus_files;
  delete shortcut_files;

  return 0;

}

bool terminate_vbs() {
  /**
   * Terminates all programs which are potential interpreters for VBScript
   * and thus potentially running Angry Birds. The terminations are forced,
   * meaning that this program may need to be run as administrator. There is
   * no way of knowing which interpreter is running Angry Birds, if any,
   * so all potential applications are terminated so that the virus will
   * not be able to reinstate itself while the filesystem is cleaned up.
   *
   * @return true if the termination of at least one program was successful.
   */

  bool success = false;
  string command;

  for (int i=0 ; i<VB_COUNT ; i++) {
    command = "taskkill /f /fi \"IMAGENAME eq ";
    command += string(vbs_programs[i]);
    command += string("\"");
    if (strcmp(exec(command.c_str()).c_str(), "ERROR") != 0) {
      success = true;
    }
  }

  return success;
}

void find_malicious(string filesystem, vector<string>* virus_files, 
    vector<string>* shortcut_files) {
  /**
   * Locates all malicious files on the given filesystem (or subset of
   * a filesystem). If filesystem is "ALL", this will cover all drives on
   * the computer, including external drives. Files are identified through
   * several different methods: first, through the identification of
   * .vbs and .vbe files containing Angry Birds content, and then through
   * the identification of .lnk files which were created by Angry Birds,
   * which can be detected from the content of those files.
   *
   * @param filesystem the root of the filesystem to be swept. ALL to 
   * sweep all filesystem.
   */

  vector<string>* drives;
  string wmic;
  
  if (filesystem == "ALL") {
    wmic = exec("wmic logicaldisk get caption");
    if (strcmp(wmic.c_str(), "ERROR") == 0) {
      cerr << "No drives found! Aborting..." << endl;
      exit(-1);
    }
    else {
      drives = split(wmic, '\n');
      drives->erase(drives->begin());
    }
  }
  else {
    find_mal_in_subdirs(filesystem, virus_files, shortcut_files);
    return;
  }

  string drive;
  for (unsigned int i=0 ; i<(drives->size() - 1) ; i++) {
    // exec(drives->at(i).c_str());
    drive = strip_copy(drives->at(i));
    find_mal_in_subdirs((drive + "\x5C"), virus_files, shortcut_files);
  }

  delete drives;
}

void find_mal_in_subdirs(string dir, vector<string>* virus_files,
    vector<string>* shortcut_files) {
  /**
   * Locates all malicious files in the given directory and all
   * subdirectories of that directory. Angry Birds viruses and shortcuts
   * are identified using content specifc to each, and defined globally.
   * as AB_ID and SC_ID.
   *
   * @param dir the target directory
   * @param mal the map of malicious files which will be added to
   */
  string potential;
  vector<string>* potential_list;
  vector<string>* potentials_2;
  string shortcuts = "";
  string viruses = "";
  vector<string>* bad_db;
  
  cout << "Changing directory to: " << dir.c_str() << endl;
  if (!SetCurrentDirectory(dir.c_str())) {
    cerr << "Could not access " << dir.c_str() << endl;
  }

  potential = exec("dir /s /b /A H S A *.lnk");
  potential_list = split(potential, '\n');
  potential = exec("dir /s /b /A H S A *.exe");
  potentials_2 = split(potential, '\n');

  for (unsigned int i=0 ; i<potentials_2->size() ; i++) {
    potential_list->push_back(potentials_2->at(i));
  }

  for (unsigned int i=0 ; i<potential_list->size() ; i++) {
    if ((string_in_file(SC_ID, SC_CHARS, potential_list->at(i).c_str()) 
        || string_in_file(EXE_ID, EX_CHARS, potential_list->at(i).c_str()))
          && !string_in_file(AB_ID, AB_CHARS, potential_list->at(i).c_str())) {
      shortcut_files->push_back(potential_list->at(i));
    }
  }

  delete potential_list;
  delete potentials_2;

  potential = exec("dir /s /b /A H S A *.vbe");
  potential_list = split(potential, '\n');
  potential = exec("dir /s /b /A H S A *.ini");
  potentials_2 = split(potential, '\n');

  for (unsigned int i=0 ; i<potentials_2->size() ; i++) {
    potential_list->push_back(potentials_2->at(i));
  }

  for (unsigned int i=0 ; i<potential_list->size() ; i++) {
    if (string_in_file(AB_ID, AB_CHARS, potential_list->at(i).c_str())
        || string_in_file(INI_ID, INI_CHARS, potential_list->at(i).c_str())) {
      virus_files->push_back(potential_list->at(i));
    }
  }

  bad_db = split(exec("dir /s /b /A H S A Thumbs.db"), '\n');
  for (unsigned int i=0 ; i<bad_db->size() ; i++) {
    DeleteFile(bad_db->at(i).c_str());
  }

  delete potential_list;
  delete potentials_2;
  delete bad_db;

}

short destroy_shortcut(string target) {
  /**
   * Deletes a malicious shortcut file created by Angry Birds and restores
   * the original file, unhiding it.
   *
   * @param target the file to be deleted
   * @return a short integer representing the error code. 1 if the deletion
   * fails, 2 if the restoration fials, and 3 if both fail.
   */

  short code = 0;

  // Delete the file. If the command fails, set code to 1
  if (DeleteFile(target.c_str())) {
    code = 1;
  }
 
  // Unhide the original file. If the command fails, set code to 3 or 2
  if (strcmp(exec(("ATTRIB -S -H \"" + remove_extension(target) + 
        "*\"").c_str()).c_str(), "ERROR") == 0) {
    if (code == 1) {
      code = 3;
    }
    else {
      code = 2;
    }
  }

  return code;
}


bool destroy_virus(string target) {
  /**
   * Deletes a malicious VBScript file containing Angry Birds
   *
   * @param target the path of the file to be deleted
   * @return a boolean representing whether the deletion was successful
   */

  if (DeleteFile(target.c_str())) {
    return false;
  }
  return true;
}

string remove_extension(string str) {
  /**
   * Removes the final extension from a string and returns
   * the modified string.
   *
   * @param string the filename whose extension should be removed
   * @return a string with no final extension
   */
  string stripped = "";
  vector<string>* tmp = split(str, '.');
  for (unsigned int i=0 ; i<(tmp->size() - 2) ; i++) {
    stripped += tmp->at(i) + ".";
  }
  stripped += tmp->at(tmp->size() - 2);

  delete tmp;
  return stripped;
}

vector<string> &split(const string &s, char delim, vector<string> &elems) {
  /**
   * This function is a helper to split(const string &s, char delim). It
   * adds elements to the vector elems by splitting the string s every time
   * delim is found.
   *
   * @param s the string to be split
   * @param delim the character delimeter at which to split s
   * @param elems the vector of split elements
   * @return the list of elements
   */
  stringstream ss(s);
  string item;
  while (getline(ss, item, delim)) {
    elems.push_back(item);
  }
  return elems;
}

vector<string>* split(const string &s, char delim) {
  /**
   * Splits a string at all instances of the given delimeter. This does not
   * skip empty tokens.
   *
   * @param s the string to be split
   * @param delim the character delimiter
   * @return a list of elements
   */
  vector<string>* elems = new vector<string>();
  split(s, delim, *elems);
  return elems;
}

string exec(const char* cmd) {
  /**
   * Execute a given command and returns the output. Outputs "ERROR" if the
   * given command fails to execute or gives an error code on exit.
   *
   * @param cmd the command to execute
   * @return standard output of cmd
   */
  cout << cmd << endl;
  FILE* pipe = popen(cmd, "r");
  if (!pipe) return "ERROR";
  char buffer[128];
  string result = "";
  while (!feof(pipe)) {
    if (fgets(buffer, 128, pipe) != NULL) {
      result += buffer;
    }
  }
  pclose(pipe);
  return result;
}

string to_upper_copy(string str) {
  /**
   * Convert a string into entirely upper case characters
   *
   * @param str the string to convert
   * @return a copy of str composed of upper case characters only.
   */

  string new_str = "";
  for (unsigned int i=0 ; i<str.size() ; i++) {
    new_str += toupper(str[i]);
  }

  return new_str;
}
 
bool string_in_file(char* to_find, int size, const char* filename) {
  /**
   * @param to_find the character array of characters to find
   * @param size the size of to_find
   * @param filename the name of the file to search
   * @return true if the given string is contained in the file.
   */
  streampos file_size;
  char* memblock = new char[1];
  int str_pos;

  cout << "Comparing strings in file " << filename << endl;

  ifstream file(filename, ios::in|ios::binary|ios::ate);
  if (file.is_open()) {
    try {
      file_size = file.tellg();
      delete memblock;
      memblock = new char[file_size];
      file.seekg(0, ios::beg);
      file.read(memblock, file_size);
      file.close();
  
      str_pos = 0;
      for (int i=0 ; i<file_size ; i++) {
        if (str_pos == size) {
          return true;
        }
        if (memblock[i] == to_find[str_pos]) {
          str_pos += 1;
        }
      }

      delete memblock;
      return false;
    }
    catch (bad_alloc &ba) {
      cerr << "Ignoring file on basis of size." << endl;
      delete memblock;
      return false;
    }
  }
  else {
    cerr << "Could not open file." << endl;
    return false;
  }
}

string strip_copy(string to_strip) {
  /**
   * Copies a string, removing all whitespace characters.
   */
  string new_str = "";

  for (unsigned int i=0 ; i<to_strip.size() ; i++) {
    if (to_strip[i] != ' ' && to_strip[i] != '\r' && to_strip[i] != '\n') {
      new_str += to_strip[i];
    }
  }

  return new_str;
}
