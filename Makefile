CXXFLAGS = -Wall -O2
CXX = i586-mingw32msvc-g++
RXX = i586-mingw32msvc-windres

SRC = ./src

DIR = ./bin

OBJS = angrypigs.res

COBJS = $(patsubst %, $(DIR)/%,$(OBJS))

all: angrypigs

angrypigs: $(SRC)/angrypigs.cpp $(OBJS)
	$(CXX) $(CXXFLAGS) $(SRC)/angrypigs.cpp $(COBJS) -o $(DIR)/angrypigs.exe

angrypigs.res:
	$(RXX) $(SRC)/angrypigs.rc -O coff -o $(DIR)/angrypigs.res

clean:
	rm -f $(DIR)/*.exe

.PHONY: clean all
